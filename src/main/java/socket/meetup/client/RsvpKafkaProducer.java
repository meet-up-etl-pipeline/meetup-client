package socket.meetup.client;


import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@SuppressWarnings("deprecation")
@Slf4j
@AllArgsConstructor
@Component
@EnableBinding(Source.class)
public class RsvpKafkaProducer {
	private static final int MESSAGE_TIMEOUT = 10000;
	private final Source source;
	
	public void sendMessage(String message) {
		source.output()
			  .send(MessageBuilder.withPayload(message).build(), MESSAGE_TIMEOUT);
		log.info("Message Sent Successfully.");
	}

}
